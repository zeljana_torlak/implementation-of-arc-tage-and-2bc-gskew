package rs.ac.bg.etf.aor.replacementpolicy;

import rs.ac.bg.etf.aor.memory.MemoryOperation;
import rs.ac.bg.etf.aor.memory.cache.ICacheMemory;
import rs.ac.bg.etf.aor.memory.cache.Tag;

import java.util.ArrayList;
import java.util.LinkedList;

public class ARCReplacementPolicy implements IReplacementPolicy {

    private class Elem {
        LinkedList<Long> t1, t2;
        LinkedList<Long> b1, b2;
        long p;

        public Elem() {
            t1 = new LinkedList<>();
            t2 = new LinkedList<>();
            b1 = new LinkedList<>();
            b2 = new LinkedList<>();
        }
    }

    protected ICacheMemory ICacheMemory;
    protected long c;
    protected Elem[] ARC;
    protected boolean learn;

    public void init(ICacheMemory cache) {
        this.ICacheMemory = cache;
        c = (long) ICacheMemory.getSetAsociativity();

        reset();
    }


    public int getBlockIndexToReplace(long adr) {
        int setAsoc = (int) ICacheMemory.getSetAsociativity();
        int set = (int) ICacheMemory.extractSet(adr);
        ArrayList<Tag> tagMemory = ICacheMemory.getTags();
        long tagData = ICacheMemory.extractTag(adr);
        //System.out.println("***miss adr:" + adr + " tag:" + tagData);
//        for (int i = 0; i < tagMemory.size(); i++) {
//            System.out.println("tag" + i + ": " + tagMemory.get(i) );
//        }

        if (ARC[set].b1.contains(tagData)) {
            ARC[set].b1.remove(tagData);
            ARC[set].p++;
            if (ARC[set].p > c) ARC[set].p = c;
            else if (ARC[set].t2.size() > (c - ARC[set].p)) {
                long temp = ARC[set].t2.removeLast();
                ARC[set].b2.add(0, tagMemory.get((int) temp).tag);
            }
        } else if (ARC[set].b2.contains(tagData)) {
            ARC[set].b2.remove(tagData);
            ARC[set].p--;
            if (ARC[set].p < 0) ARC[set].p = 0;
            else if (ARC[set].t1.size() > ARC[set].p) {
                long temp = ARC[set].t1.removeLast();
                ARC[set].b1.add(0, tagMemory.get((int) temp).tag);
            }
            //learn = true;
        }

        if (ARC[set].t1.size() < ARC[set].p) {
            for (int i = 0; i < setAsoc; i++) {
                if (!(ARC[set].t1.contains((long) (set * setAsoc + i)) || ARC[set].t2.contains((long) (set * setAsoc + i)))) {
                    //System.out.println(" ***block1:" + (set * setAsoc + i));
                    return set * setAsoc + i;
                }
            }
        }

        if (ARC[set].t1.size() > 0) {
            long ret = ARC[set].t1.removeLast();
            ARC[set].b1.add(0, tagMemory.get((int) ret).tag);
            if (ARC[set].b1.size() > (c - ARC[set].p))
                ARC[set].b1.removeLast();
            //System.out.println("***block2:" + (ret));
            return (int) ret;
        } else {
            long ret = ARC[set].t2.removeLast();
            ARC[set].b2.add(0, tagMemory.get((int) ret).tag);
            if (ARC[set].b2.size() > ARC[set].p)
                ARC[set].b2.removeLast();
            //System.out.println("***block2:" + (ret));
            return (int) ret;
        }
    }

    @Override
    public void doOperation(MemoryOperation operation) {
        MemoryOperation.MemoryOperationType opr = operation.getType();
        long adr = operation.getAddress();

        if ((opr == MemoryOperation.MemoryOperationType.READ) || (opr == MemoryOperation.MemoryOperationType.WRITE)) {
            int set = (int) ICacheMemory.extractSet(adr);
            int setAsoc = (int) ICacheMemory.getSetAsociativity();
            long tagData = ICacheMemory.extractTag(adr);
            ArrayList<Tag> tagMemory = ICacheMemory.getTags();
            //System.out.println("***hit adr:" + operation.getAddress() + " tag:" + tagData);
//            for (int i = 0; i < tagMemory.size(); i++) {
//                System.out.println("tag" + i + ": " + tagMemory.get(i) );
//            }

            for (int i = 0; i < setAsoc; i++) {
                long block = set * setAsoc + i;
                Tag tag = tagMemory.get((int) block);
                if (tag.tag == tagData) {
                    if (ARC[set].t1.contains(block)) {
                        ARC[set].t1.remove(block);
                        ARC[set].t2.add(0, block);
                        if (ARC[set].t2.size() > (c - ARC[set].p)) {
                            long temp = ARC[set].t2.removeLast();
                            ARC[set].b2.add(0, tagMemory.get((int) temp).tag);
                            if (ARC[set].b2.size() > ARC[set].p)
                                ARC[set].b2.removeLast();
                        }
                        return;
                    } else if (ARC[set].t2.contains(block)) {
                        ARC[set].t2.remove(block);
                        ARC[set].t2.add(0, block);
                        return;
                    }
                    break;
                }
            }

            long block = set * setAsoc;
            for (int i = 0; i < setAsoc; i++) {
                Tag tag = tagMemory.get(set * setAsoc + i);
                if (tag.V && tag.tag == tagData) {
                    block = set * setAsoc + i;
                    break;
                }
            }

            /*if (learn) {
                learn = false;
                ARC[set].t2.add(0, block);
                if (ARC[set].t2.size() > (c - ARC[set].p)) {
                    long temp = ARC[set].t2.removeLast();
                    ARC[set].b2.add(0, tagMemory.get((int) temp).tag);
                    if (ARC[set].b2.size() > ARC[set].p)
                        ARC[set].b2.removeLast();
                }
            } else {*/
            if (ARC[set].t1.size() < ARC[set].p)
                ARC[set].t1.add(0, block);
            else
                ARC[set].t2.add(0, block);

            //}

        } else if (opr == MemoryOperation.MemoryOperationType.FLUSHALL) {
            for (int i = 0; i < ARC.length; i++) {
                Elem elem = ARC[i];
                elem.t1 = new LinkedList<>();
                elem.t2 = new LinkedList<>();
                elem.b1 = new LinkedList<>();
                elem.b2 = new LinkedList<>();
                ARC[i] = elem;
                learn = false;
            }
        }

    }


    public String printAll() {
        StringBuilder s = new StringBuilder();
        int size = ARC.length;
        for (int i = 0; i < size; i++) {
            s = s.append("Set ").append(i).append(", p ").append(ARC[i].p).append("\n").append(", Top of list 1 ");
            for (int j = 0; j < ARC[i].t1.size(); j++) {
                s = s.append(ARC[i].t1.get(j)).append(" ");
            }
            s = s.append(", Top of list 2 ");
            for (int j = 0; j < ARC[i].t2.size(); j++) {
                s = s.append(ARC[i].t2.get(j)).append(" ");
            }
            s = s.append("\n");
        }
        return s.toString();
    }

    @Override
    public void reset() {
        int size = (int) ICacheMemory.getSetNum();
        ARC = new Elem[size];
        learn = false;
        for (int i = 0; i < size; i++) {
            Elem novi = new Elem();
            if (c == 1) novi.p = 1;
            else novi.p = c / 2;
            ARC[i] = novi;
        }
    }

    public String printValid() {
        return printAll();
    }
}

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define min(x,y) (((x) < (y)) ? (x) : (y))
#define B 5
#define N 23

void pocetak(){}
void kraj(){}

int main()
{
	pocetak();
	srand(time(NULL));

	int i, j, k, jj, kk;
	int x[N][N],  y[N][N],  z[N][N], a[N][N], b[N][N], c[N][N], d[N][N];
	int r = 0;

	for (j = 0; j < N; j = j+1) 
		for (i = 0; i < N; i = i+1){
			x[i][j] = rand();
			y[i][j] = rand();
			z[i][j] = rand();
			a[i][j] = rand();
			b[i][j] = rand();
			c[i][j] = rand();
			d[i][j] = rand();
		};
 
	for (k = 0; k < N; k = k+1) 
		for (j = 0; j < N; j = j+1) 
			for (i = 0; i < N; i = i+1) 
				x[i][j] = 2 * x[i][j]; 
 
	for (k = 0; k < N; k = k+1) 
		for (i = 0; i < N; i = i+1) 
			for (j = 0; j < N; j = j+1) 
				x[i][j] = 2 * x[i][j]; 


	for (i = 0; i < N; i = i+1) 
		for (j = 0; j < N; j = j+1) 
			a[i][j] = 1/b[i][j] * c[i][j]; 
	for (i = 0; i < N; i = i+1) 
		for (j = 0; j < N; j = j+1) 
			d[i][j] = a[i][j] + c[i][j]; 

	for (i = 0; i < N; i = i+1) 
		for (j = 0; j < N; j = j+1) { 
			a[i][j] = 1/b[i][j] * c[i][j]; 
			d[i][j] = a[i][j] + c[i][j];
		};

	for (i = 0; i < N; i = i+1) 
		for (j = 0; j < N; j = j+1) {
			r = 0; 
			for (k = 0; k < N; k = k+1){ 
				r = r + y[i][k]*z[k][j];}; 
			x[i][j] = r; 
		}; 
	 
	for (jj = 0; jj < N; jj = jj+B) 
		for (kk = 0; kk < N; kk = kk+B) 
			for (i = 0; i < N; i = i+1) 
				for (j = jj; j < min(jj+B-1,N); j = j+1) {
					r = 0; 
					for (k = kk; k < min(kk+B-1,N); k = k+1) { 
						r = r + y[i][k]*z[k][j];}; 
					x[i][j] = x[i][j] + r; 
				};

	kraj();

	return 0;
}

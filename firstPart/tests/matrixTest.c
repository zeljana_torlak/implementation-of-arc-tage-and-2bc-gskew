#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define min(x,y) (((x) < (y)) ? (x) : (y))

void pocetak(){}
void kraj(){}

int main()
{
	pocetak();
	srand(time(NULL));

	int i, j, k, jj, kk;
	int B = 5;
	int x[23][23],  y[23][23],  z[23][23], a[23][23], b[23][23], c[23][23], d[23][23];
	int r = 0;

	for (j = 0; j < 23; j = j+1) 
		for (i = 0; i < 23; i = i+1){
			x[i][j] = rand();
			y[i][j] = rand();
			z[i][j] = rand();
			a[i][j] = rand();
			b[i][j] = rand();
			c[i][j] = rand();
			d[i][j] = rand();
		};
 
	for (k = 0; k < 23; k = k+1) 
		for (j = 0; j < 23; j = j+1) 
			for (i = 0; i < 23; i = i+1) 
				x[i][j] = 2 * x[i][j]; 
 
	for (k = 0; k < 23; k = k+1) 
		for (i = 0; i < 23; i = i+1) 
			for (j = 0; j < 23; j = j+1) 
				x[i][j] = 2 * x[i][j]; 


	for (i = 0; i < 23; i = i+1) 
		for (j = 0; j < 23; j = j+1) 
			a[i][j] = 1/b[i][j] * c[i][j]; 
	for (i = 0; i < 23; i = i+1) 
		for (j = 0; j < 23; j = j+1) 
			d[i][j] = a[i][j] + c[i][j]; 

	for (i = 0; i < 23; i = i+1) 
		for (j = 0; j < 23; j = j+1) { 
			a[i][j] = 1/b[i][j] * c[i][j]; 
			d[i][j] = a[i][j] + c[i][j];
		};

	for (i = 0; i < 23; i = i+1) 
		for (j = 0; j < 23; j = j+1) {
			r = 0; 
			for (k = 0; k < 23; k = k+1){ 
				r = r + y[i][k]*z[k][j];}; 
			x[i][j] = r; 
		}; 
	 
	for (jj = 0; jj < 23; jj = jj+B) 
		for (kk = 0; kk < 23; kk = kk+B) 
			for (i = 0; i < 23; i = i+1) 
				for (j = jj; j < min(jj+B-1,23); j = j+1) {
					r = 0; 
					for (k = kk; k < min(kk+B-1,23); k = k+1) { 
						r = r + y[i][k]*z[k][j];}; 
					x[i][j] = x[i][j] + r; 
				};

	kraj();

	return 0;
}

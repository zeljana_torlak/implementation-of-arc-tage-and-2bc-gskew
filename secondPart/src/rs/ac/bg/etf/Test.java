package rs.ac.bg.etf;

import rs.ac.bg.etf.automaton.Automaton;
import rs.ac.bg.etf.parser.CisPenn2011.CISPENN2011_Parser;
import rs.ac.bg.etf.parser.MyValgrindParser.MyValgrindParser;
import rs.ac.bg.etf.parser.MyXzTraceParser.MyXZTraceParser;
import rs.ac.bg.etf.parser.Parser;
import rs.ac.bg.etf.predictor.Instruction;
import rs.ac.bg.etf.predictor.Predictor;
import rs.ac.bg.etf.predictor.Predictor_2bc_gskew.Predictor_2bc_gskew;
import rs.ac.bg.etf.predictor.Predictor_TAGE.Predictor_TAGE;
import rs.ac.bg.etf.predictor.bimodal.Bimodal;
import rs.ac.bg.etf.predictor.correlation.Correlation;
import rs.ac.bg.etf.predictor.twoLevel.TwoLevel;
import rs.ac.bg.etf.stats.Statistics;

import java.io.FileWriter;
import java.io.IOException;

public class Test {

	private static String statsFilePath = "./traces/stats.txt";


	public static void main(String[] args) {

		String pathToTrace = args.length>0?args[0]: "./traces/gcc-50M.trace.gz"; //"./traces/cmon.txt";

		Predictor predictor=null;

		//predictor = new TwoLevel(10, Automaton.AutomatonType.TWOBITS_TYPE1);
		//predictor = new Correlation(10,3, Automaton.AutomatonType.TWOBITS_TYPE2);
		//predictor = new Bimodal(10,5,6, Automaton.AutomatonType.TWOBITS_TYPE3);
		//predictor = new Predictor_TAGE(10, 10, 0.6, 2);
		//predictor = new Predictor_2bc_gskew(10,5,6, Automaton.AutomatonType.TWOBITS_TYPE3, 2);


		int bhrBits = 0;
		int addrBits = 0;
		Automaton.AutomatonType[] automata = {
				Automaton.AutomatonType.ONEBIT,
				Automaton.AutomatonType.TWOBITS_TYPE1,
				Automaton.AutomatonType.TWOBITS_TYPE2,
				Automaton.AutomatonType.TWOBITS_TYPE3,
				Automaton.AutomatonType.TWOBITS_TYPE4};


		Automaton.AutomatonType automatonType = Automaton.AutomatonType.ONEBIT;

		double[] alphas = { 0, 1, 2};

		/*for (int i = 0; i < 3; i++) {
			bhrBits = (i+1)*5;

			for (int k = 0; k < 5; k++) {
				automatonType = automata[k];

				predictor = new TwoLevel(bhrBits, automatonType);
				parse(pathToTrace, bhrBits, 0, automatonType, predictor, "TwoLevel");
			}

		}*/

		/*for (int i = 0; i < 3; i++) {
			bhrBits = (i+1)*5;

			for (int j = 0; j < 3; j++) {
				addrBits = (j+1)*3;

				for (int k = 0; k < 5; k++) {
					automatonType = automata[k];

					predictor = new Correlation(bhrBits, addrBits, automatonType);
					parse(pathToTrace, bhrBits, addrBits, automatonType, predictor, "Correlation");
				}

			}
		}*/

		/*for (int i = 0; i < 3; i++) {
			bhrBits = (i+1)*5;

			for (int j = 0; j < 3; j++) {
				addrBits = (j+1)*3;

				for (int k = 0; k < 5; k++) {
					automatonType = automata[k];

					predictor = new Bimodal(bhrBits, addrBits, 6, automatonType);
					parse(pathToTrace, bhrBits, addrBits, automatonType, predictor, "Bimodal");
				}

			}
		}*/

		for (int i = 0; i < 3; i++) {
			bhrBits = (i+1)*5;

			for (int j = 0; j < 3; j++) {
				addrBits = (j+1)*3;

				for (int k = 0; k < alphas.length; k++) {

					predictor = new Predictor_TAGE(bhrBits, addrBits, alphas[k],2);
					parse(pathToTrace, bhrBits, addrBits, automatonType, predictor, "TAGE");

				}

			}
		}

		/*for (int i = 0; i < 3; i++) {
			bhrBits = (i+1)*5;

			for (int j = 0; j < 3; j++) {
				addrBits = (j+1)*3;

				for (int k = 0; k < 5; k++) {
					automatonType = automata[k];

					predictor = new Predictor_2bc_gskew(bhrBits, addrBits,6, automatonType,2);
					parse(pathToTrace, bhrBits, addrBits, automatonType, predictor, "2bc_gskew");
				}

			}
		}*/

	}



	private static void test(Parser parser, Predictor predictor) {

		Statistics stats = new Statistics();


		long start = System.currentTimeMillis();
		System.out.println("Start!");

		Instruction ins;
		int all = 0;
		while ((ins = parser.getNext()) != null) {
			all++;
//				if (!ins.isConditional()) {
//					continue;
//				}
			if (!ins.isBranch())
				continue;
			boolean prediction = predictor.predict(ins);
			if (prediction != ins.isTaken())
				stats.incNumOfMisses();
			else
				stats.incNumOfHits();
			stats.incNumOfCondBranches();

			predictor.update(ins);
		}

		long end = System.currentTimeMillis();
		System.out.println("End!");

		long durationInMillis = end - start;
		long millis = durationInMillis % 1000;
		long second = (durationInMillis / 1000) % 60;
		long minute = (durationInMillis / (1000 * 60)) % 60;
		long hour = (durationInMillis / (1000 * 60 * 60)) % 24;

		String time = String.format("%02d:%02d:%02d.%d", hour, minute, second, millis);

		System.out.println("Duration: " + time);
		System.out.println("Hits: " + stats.getNumOfHits());
		System.out.println("Misses: " + stats.getNumOfMisses());
		double percent = (double) stats.getNumOfHits() / stats.getNumOfCondBranches() * 100;
		System.out.println("Percent of hits: " + percent);
		System.out.println("Sum: " + stats.getNumOfCondBranches());
		int sum = stats.getNumOfHits() + stats.getNumOfMisses();
		System.out.println("Sum check: " + sum);
		System.out.println("All: " + all);


		String statsLine = String.format("%d\t%d\t%f\n", stats.getNumOfHits(), stats.getNumOfMisses(), percent);
		writeStatsLine(statsLine, statsFilePath);

	}

	private static void writeStatsLine(String line, String StatsFilePath){


		try (FileWriter fw = new FileWriter(StatsFilePath, true)){

			fw.append(line);

		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	private static void parse(String pathToTrace, int bhrBits, int addrBits, Automaton.AutomatonType automatonType, Predictor predictor, String name){
		//Parser parser = new MyValgrindParser(pathToTrace);
		Parser parser = new CISPENN2011_Parser(pathToTrace);
		System.out.println(bhrBits + " " + addrBits + " " + automatonType.name() + " " + name);
		String line = String.valueOf(bhrBits) + "	" + String.valueOf(addrBits) + "	" + automatonType.name() + "	" + name + "	";
		writeStatsLine(line, statsFilePath);
		test(parser, predictor);
	}

}
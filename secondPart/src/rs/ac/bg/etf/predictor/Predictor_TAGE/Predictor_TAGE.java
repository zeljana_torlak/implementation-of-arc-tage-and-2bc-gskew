package rs.ac.bg.etf.predictor.Predictor_TAGE;

import rs.ac.bg.etf.predictor.BHR;
import rs.ac.bg.etf.predictor.Instruction;
import rs.ac.bg.etf.predictor.Predictor;

public class Predictor_TAGE implements Predictor {

    BHR bhr;
    int mask;

    int[][] counter;
    int[][] tag;
    int[][] U;

    int[] elementary;
    int[] L;

    double alpha;

    int col1, row1; //provider
    int col2, row2; //alternative provider

    public Predictor_TAGE(int BHRsize, int numOfLastBitsAddress, double alpha, int Lstart){
        bhr = new BHR(BHRsize);

        int rowSize = 1<<numOfLastBitsAddress;
        mask = rowSize-1;

        counter = new int[4][];
        tag = new int[4][];
        U = new int[4][];
        elementary = new int[rowSize];

        for (int i = 0; i < 4; i++) {
            counter[i] = new int[rowSize];
            tag[i] = new int[rowSize];
            U[i] = new int[rowSize];
            for (int j = 0; j < rowSize; j++) {
                counter[i][j] = 0;
                tag[i][j] = -1;
                U[i][j] = 0;
                elementary[j] = 0;
            }
        }

        this.alpha = alpha;
        L = new int[4];
        if (Lstart <= 0) Lstart = 1;
        L[0] = Lstart;
        L[1] = (int)(alpha * L[0] + 0.5);
        L[2] = (int)(alpha * alpha * L[0] + 0.5);
        L[3] = (int)(alpha * alpha * alpha * L[0] + 0.5);

        col1 = -1; row1 = -1;
        col2 = -1; row2 = -1;
    }

    @Override
    public boolean predict(Instruction branch) {

        for (int i = 0; i < 4; i++) {
            int Lmask = 1;
            for (int j = 0; j < (L[i]-1); j++) {
                Lmask <<= 1;
                Lmask |= 1;
            }
            int find = ((int)((bhr.getValue() & Lmask) * branch.getAddress())) & mask;
            for (int j = 0; j < tag[i].length; j++) {
                if (tag[i][j] == find){
                    if (col1 != -1){
                        col2 = col1; row2 = row1;
                    }
                    col1 = i; row1 = j;
                    break;
                }
            }
        }

        if (col1 == -1) return (elementary[(int) (branch.getAddress() & mask)] >= 0);
        return (counter[col1][row1] >= 0);
    }

    @Override
    public void update(Instruction branch) {
        boolean outcome = branch.isTaken();

        boolean providerPrediction;
        boolean alterntiveProviderPrediction;
        if (col1 == -1){
            providerPrediction = (elementary[(int) (branch.getAddress() & mask)] >= 0 ? true : false);
        } else {
            providerPrediction = (counter[col1][row1] >= 0 ? true : false);
        }
        if (col2 == -1){
            alterntiveProviderPrediction = (elementary[(int) (branch.getAddress() & mask)] >= 0 ? true : false);
        } else {
            alterntiveProviderPrediction = (counter[col2][row2] >= 0 ? true : false);
        }

        if (providerPrediction != alterntiveProviderPrediction){
            if (providerPrediction == outcome) {
                if (col1 != -1){
                    U[col1][row1]++;
                }
            }
            else {
                if (col1 != -1){
                    U[col1][row1]--;
                }
            }
        }

        if(outcome != providerPrediction) {
            boolean ok = false;
            for (int i = 3; i >= 0; i--) {
                for (int j = 0; j < U[i].length; j++) {
                    if (U[i][j] == 0) {
                        int Lmask = 1;
                        for (int k = 0; k < (L[i]-1); k++) {
                            Lmask <<= 1;
                            Lmask |= 1;
                        }
                        tag[i][j] = ((int)((bhr.getValue() & Lmask) * branch.getAddress())) & mask;
                        counter[i][j] = (outcome ? 0 : -1);
                        ok = true;
                        break;
                    }
                }
                if (ok) break;
            }
            if (!ok)
                for (int i = 0; i < 4; i++) {
                    for (int j = 0; j < U[i].length; j++) {
                        U[i][j]--;
                    }
                }
        }

        if (col1 == -1){
            if (outcome)
                elementary[(int) (branch.getAddress() & mask)]++;
            else
                elementary[(int) (branch.getAddress() & mask)]--;
        } else {
            if (outcome)
                counter[col1][row1]++;
            else
                counter[col1][row1]--;
        }
        bhr.insertOutcome(outcome);

        col1 = -1; row1 = -1;
        col2 = -1; row2 = -1;
    }
}

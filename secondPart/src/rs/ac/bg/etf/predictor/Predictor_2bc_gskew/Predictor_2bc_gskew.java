package rs.ac.bg.etf.predictor.Predictor_2bc_gskew;

 import rs.ac.bg.etf.automaton.Automaton;
 import rs.ac.bg.etf.predictor.BHR;
 import rs.ac.bg.etf.predictor.Instruction;
 import rs.ac.bg.etf.predictor.Predictor;
 import rs.ac.bg.etf.predictor.bimodal.Bimodal;

public class Predictor_2bc_gskew implements Predictor {

    Automaton[][] automatons;
    Bimodal bimodal;
    boolean[] meta;

    BHR bhr;
    int maskGroup;

    boolean bimodalPrediction;
    boolean e_gskewPrediction;
    boolean finalPrediction;

    public Predictor_2bc_gskew(int BHRsize, int numOfLastBitsAddressGroups, int numOfLastBitsAddressSelectorBimodal, Automaton.AutomatonType type, int historySizeBimodal){
        bhr = new BHR(BHRsize);
        bimodal = new Bimodal(BHRsize, numOfLastBitsAddressGroups, numOfLastBitsAddressSelectorBimodal, type, historySizeBimodal);

        int rowSizeGroup = 1<<(BHRsize>numOfLastBitsAddressGroups?BHRsize:numOfLastBitsAddressGroups);
        maskGroup = rowSizeGroup-1;

        automatons = new Automaton[2][];
        automatons[0] = Automaton.instanceArray(type,rowSizeGroup);
        automatons[1] = Automaton.instanceArray(type,rowSizeGroup);

        meta = new boolean[rowSizeGroup];
        for (int i = 0; i < meta.length; i++) {
            meta[i] = true;
        }
    }

    @Override
    public boolean predict(Instruction branch) {
        int f0 = (int) ((bhr.getValue() * branch.getAddress())&maskGroup);
        int f1 = (int) ((bhr.getValue() ^ branch.getAddress())&maskGroup);
        int f2 = (int) (((bhr.getValue() * branch.getAddress()) ^ bhr.getValue())&maskGroup);

        bimodalPrediction = bimodal.predict(branch);
        int e_gskew = (bimodalPrediction ? 1 : 0);
        if (automatons[0][f1].predict())
            e_gskew++;
        if (automatons[1][f2].predict())
            e_gskew++;
        e_gskewPrediction = (e_gskew > 1 ? true : false);

        finalPrediction = (meta[f0] ? e_gskewPrediction : bimodalPrediction);
        return finalPrediction;
    }

    @Override
    public void update(Instruction branch) {
        int f0 = (int) ((bhr.getValue() * branch.getAddress())&maskGroup);
        int f1 = (int) ((bhr.getValue() ^ branch.getAddress())&maskGroup);
        int f2 = (int) (((bhr.getValue() * branch.getAddress()) ^ bhr.getValue())&maskGroup);

        boolean outcome = branch.isTaken();

        if (meta[f0]){
            if (bimodal.predict(branch) == finalPrediction)bimodal.update(branch);
            if (automatons[0][f1].predict() == finalPrediction) automatons[0][f1].updateAutomaton(outcome);
            if (automatons[1][f2].predict() == finalPrediction)automatons[1][f2].updateAutomaton(outcome);
        } else
            bimodal.update(branch);

        if (bimodalPrediction != e_gskewPrediction)
            if (meta[f0] && (e_gskewPrediction != outcome)) meta[f0] = false;
            if (!meta[f0] && (bimodalPrediction != outcome)) meta[f0] = true;

        bhr.insertOutcome(outcome);
    }
}
